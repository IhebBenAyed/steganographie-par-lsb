package stegano;

/**
 * SteganoLSB : est une class qui lit tous les bits d'une image Bitmap(.bmp) et
 * dissimule un message provenant d'un fichier texte (.txt) au sein de cette image 
 * en utilisant la méthode LSB ; c'est une méthode qui sert à modifier
 * les bits de poids faible des pixels par un bout de lettre du message.
 */

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.JOptionPane;


public class SteganoLSB {
	
	private byte[] data;	// tableau contenant les pixels de l'image
	private FileOutputStream fos;
	
	private String filename;
	
	/**
	 * constructeur de la class(Fichier à lire)
	 * @param filename : nom du fichier qu'on va lire (ou bien son chemin)
	 * @throws IOException File not available 
	 */
	public SteganoLSB(String filename)
	{
		this.filename = filename;
		try {
			data = Files.readAllBytes(Paths.get(filename));			
		} catch (IOException e)
		{
			new JOptionPane("Fichier introuvable !").setVisible(true);
		}
	}
	
	/**
	 * redTextFile : cette methode lit un fichier (.txt) et retourne le texte qu'il contient. 
	 * @param filename :  nom du fichier qu'on va lire (ou bien son chemin)
	 * @return contenu du fichier (.txt) dans une chaine .
	 * @throws IOException File not available 
	 */
	public String readTextFile(String filename) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			String ligne;
			StringBuffer fichier = new StringBuffer();
 
			while((ligne = reader.readLine()) != null){
				fichier.append(ligne);
				fichier.append("\n");			
			}
			reader.close();
 
			return fichier.toString();		
		} catch (IOException e) {
			return e.getMessage();
		}
	}
	
	/**
	 * textIntoImage : fait dissimuler le texte dans l'image
	 * @param message : le message à dissimuler
	 * @throws IOException File not available
	 */
	public void textIntoImage(String message)
	{
		
		int currentByte = 60; // on commence du 60ème pixel
		byte []messageBytes = message.getBytes(Charset.forName("UTF-8")); // tableau qui contient le message en binaire		
		int messageLength = messageBytes.length; 
		
		//dissimuler la longueur du message
		for(int i=31; i>=0; i--)
		{
			int x = (messageLength >>> i) & 0x01; // affecter le bit à x
			
			currentByte ++; // saut d'un octet ( une couleur)
			data[currentByte] = (byte)((data[currentByte] & (0xFE) | x));
			
		}
		
		//dissimuler le message
		for(int i=0; i < messageLength; i++)
		{
			int curchar = messageBytes[i]; // curchar : current char (caractère courant à dissumiler) 
			for(int bit=7; bit>=0;--bit)
			{
				int x = (curchar >>> bit) & 0x01; //affecter le bit à x
				
				currentByte ++; // saut d'un octet
				data[currentByte] = (byte)((data[currentByte] & (0xFE) | x)); //sauvgarder le nouveau 
				
			}
		}
		
		//mettre l'image à jour avec le message dissimuler
		try {
			StringBuilder fileout = new StringBuilder();
			fileout.append(filename);
			fileout.insert(filename.length()-4, "(cryptée)");
			fos = new FileOutputStream(fileout.toString());
			fos.write(data);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		finally{
			try {
				fos.close();
			} catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * messageIntoFile : lit le message dissimuler dans l'image et le sauvgarde dans un nouveau fichier(.txt)
	 * @return String of the message
	 */
	public void messageIntoFile()
	{
	
		int currentByte = 60;
		int messageLenght = 0;
		
		//Lire la longueur du message
		for(int i=31; i>= 0; i--)
		{
			
			currentByte++;
			messageLenght |= ((data[currentByte] & 0x01) << i);
			
		}
		
		//en cas de binaire signé on risque d'avoir une longueur negative
		if(messageLenght < 0) 
			messageLenght = -messageLenght;
		
		byte []message = new byte[messageLenght];
		
		//Lire le message
		for(int i=0; i < messageLenght; i++)
		{
			message[i] = 0;
			for(int bit = 7; bit >=0; bit--)
			{				
								
				currentByte ++;
				message[i] |= ((data[currentByte] & 0x01) << bit);
				data[currentByte] &= 0x00 ;
				
			}			
		}
		//créer le nouveau fichier (.txt) contenant le message dissimulé		
		try {
			StringBuilder fileout = new StringBuilder();
			fileout.append(filename);
			fileout.insert(filename.length(), ".txt");
			fos = new FileOutputStream(fileout.toString());
			fos.write(message);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		finally{
			try {
				fos.close();
			} catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	
	}
	
	
	
	
}
