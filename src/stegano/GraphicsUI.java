package stegano;


import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;



public class GraphicsUI extends JFrame

{
	
	private static final long serialVersionUID = 123456L;
	
	
	private int width;
	private int height;
	private JTextField textField;
	private JTextField textField1;
	private SteganoLSB lsb;
	private String filename;
	private String filename1;
	private FileDialog browser;
	private FileDialog browser1;
	private String str = new String("Bienvenu! :D\n\nLa stéganographie est l'art de la dissimulation:\n"
									+ " son objet est de faire passer inaperçu un message\n"
									+ " dans un autre message.");

	
	
	public GraphicsUI()
	
	{
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		width = (int)screenSize.getWidth();
		height = (int)screenSize.getHeight();
		
		//Réglage de la taille , emplacement , nom ..
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(400, 250);	
		setLocation((int) (width/3), (int) (height/3));
		setTitle("Steganographie"); 
		setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("ensi.png")));
		setResizable(false);
		getContentPane().setLayout(null);
		
		
		
		
		//La zone du texte 
		final JTextArea textArea = new JTextArea(str);
		textArea.setBounds(10, 25, 280, 100);
		textArea.setColumns(30);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		getContentPane().add(textArea);
		
	
		 
		
		
		// Le bouton 'Decoder'
		JButton read = new JButton("Decoder");
		read.setBounds(295, 45, 90, 25);
		getContentPane().add(read);
		read.addActionListener(new ActionListener() {
			
			
			public void actionPerformed(ActionEvent arg0) {
				
				filename = textField.getText().replace("\\", "\\\\");
				lsb = new SteganoLSB(filename);
				lsb.messageIntoFile();
				textArea.setText("Message lu avec succès ;) \n\n Votre fichier a été créer.");
			}
		});
		
		
		// Le bouton 'Coder'
		JButton write = new JButton("Coder");
		write.setBounds(295, 80, 90, 25);
		getContentPane().add(write);
		write.addActionListener(new ActionListener() {
			
			
			public void actionPerformed(ActionEvent arg0) {
				
				filename = textField.getText().replace("\\", "\\\\");
				filename1 = textField1.getText().replace("\\", "\\\\");
				lsb = new SteganoLSB(filename);
				String input= lsb.readTextFile(filename1);
				lsb.textIntoImage(input);
				textArea.setText("Message dissimulé avec succès ;) \n\n Votre image a été crée.");
								
								
			}
		});
		
		
		//Le champ du chemin de l'image
		textField = new JTextField();
		textField.setBounds(10, 186, 280, 21);
		getContentPane().add(textField);
		textField.setColumns(10);
		addWindowFocusListener(new WindowFocusListener() {
			
			
			public void windowLostFocus(WindowEvent arg0) {}
			
			
			public void windowGainedFocus(WindowEvent arg0) {
				
				try {
					filename = (browser.getDirectory() + browser.getFile()).replace("\\", "\\\\");
					textField.setText(browser.getDirectory()+browser.getFile());
				} catch(NullPointerException e)
				{
					textField.setText("Entrer votre fichier image");
				}				
			}
		});
		
		//le bouton 'ouvrir' pour image
		JButton open = new JButton("Ouvrir");
		open.setBounds(295, 185, 89, 23);
		getContentPane().add(open);
		open.addActionListener(new ActionListener() {
			
		
			public void actionPerformed(ActionEvent e) {
				
				browser = new FileDialog(GraphicsUI.this);
				browser.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("ensi.png")));
				browser.setFile("*.bmp");
				browser.setMultipleMode(false);
				browser.setTitle("Choisir un fichier");
				browser.setResizable(false);
				browser.setVisible(true);				
				
			}
		});
		
		//Le champ du chemin pour texte
		textField1 = new JTextField();
		textField1.setBounds(10, 151, 280, 21);
		getContentPane().add(textField1);
		textField1.setColumns(10);
		addWindowFocusListener(new WindowFocusListener() {
					
					
			public void windowLostFocus(WindowEvent arg0) {}
					
					
			public void windowGainedFocus(WindowEvent arg0) {
						
				try {
					filename1 = (browser1.getDirectory() + browser1.getFile()).replace("\\", "\\\\");
					textField1.setText(browser1.getDirectory()+browser1.getFile());
				} catch(NullPointerException e)
				{
					textField1.setText("Entrer votre fichier texte");
				}				
			}
		});
				
		// Le bouton 'ouvrir' pour texte
		JButton open1 = new JButton("Ouvrir");
		open1.setBounds(295, 150, 89, 23);
		getContentPane().add(open1);
		open1.addActionListener(new ActionListener() {
					
				
			public void actionPerformed(ActionEvent e) {
						
				browser1 = new FileDialog(GraphicsUI.this);
				browser1.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("ensi.png")));
				browser1.setFile("*.txt");
				browser1.setMultipleMode(false);
				browser1.setTitle("Choisir un fichier");
				browser1.setResizable(false);
				browser1.setVisible(true);				
						
			}
		});
		
		
	}
	

	
	
}

