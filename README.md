## Application de Stéganographie par LSB

Stéganographie par substitution de LSB (LSB Replacement)
Le principe de cette technique consiste à substituer les bits de poids faibles (les LSB : Least Significant Bit , bit le plus à droite dans la représentation en base 2 d’un nombre ) des pixels par les bits de message à insérer. Petite remarque : En effet, 00011110 & 11111100 (=28 - 22) donnera 00011100, et il suffit ensuite de rajouter les deux bits intéressants (ou de faire une opération de OU binaire — ça revient au même).